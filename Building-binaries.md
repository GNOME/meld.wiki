## Getting Meld code

Meld is hosted on GNOME's Gitlab instance. See https://wiki.gnome.org/Git/Developers#Getting_Code for ways to clone from GNOME's Gitlab, but you can get started with:
```
git clone https://gitlab.gnome.org/GNOME/meld.git
```


## Building current Meld on Windows

Meld has migrated to using an msys2 environment on Gitlab CI for building Windows binaries.

With a working msys2 environment, clone the Meld repository as above. From your local checkout, reproducing the steps in the `mingw32-dist` and `mingw-common` sections of https://gitlab.gnome.org/GNOME/meld/blob/master/.gitlab-ci.yml should give you an installable MSI.


## Building pre-3.20 Meld on Windows

As of version 3.11.1, Meld supports building on Windows.


### Prerequisites

 * [Python 3.4.7 x86 (32-bit)](https://www.python.org/downloads/windows/) (prior to Meld 3.18, Python 2.7.6)
 * [PyGObject Win32](http://sourceforge.net/projects/pygobjectwin32/files/)
 * [msysgit](http://msysgit.github.io/)
 * [cx_Freeze](https://pypi.python.org/pypi/cx_Freeze)

Check the SOURCES file in the [binary download directory](https://download.gnome.org/binaries/win32/meld/) for a binary release to see what versions of the above were used. PyGObject Win32 is the dependency most likely to cause issues if using a different version than that which has been tested. msysgit is used purely for the scripting environment, so any version should be fine.


### Building

Almost nothing in Meld actually requires building; we are just gathering and
packaging the necessary files for Meld to run once installed. Ideally,
creating a new Meld MSI should be:

```
python3 setup_win32.py bdist_msi
```

Some current build system limitations stop things from being this easy:

 * GSettings schemas need a manual compile first. Locate your PyGObjectWin32
   install folder (probably C:\Python36\Libs\site-packages\gnome) and find
   `glib-compile-schemas` within it. Run `glib-compile-schemas` on
   the `data` folder to generate `gschemas.compiled`.
 * Translations are not compiled or installed, and thus won't work
 * Help is not built or installed


### Installing

The resulting MSI file will be in the `dist` folder.
