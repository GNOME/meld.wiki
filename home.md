# Meld

Meld is a visual diff and merge tool for files, folders and version control.


## Notable Features

 * Two- and three-way comparison of files and directories
 * Dynamically updating comparisons
 * Support for many version control systems


## Getting Meld

 * [Meld homepage](http://meldmerge.org/)
 * [Download releases](http://download.gnome.org/sources/meld/)
 * [Download windows binaries](https://download.gnome.org/binaries/win32/meld/)


## Using Meld

 * [Meld user manual](http://meldmerge.org/help/)
 * [Running Meld on Windows](Running Meld on Windows)
 * [Running Meld on macOS](Running Meld on macOS)

### Integration with other tools

 * Git can use Meld to resolve conflicts via the `git mergetool` command
 * Will Manley has contributed a tool to let Meld compare arbitrary Git commits: https://github.com/wmanley/git-meld
 * Meld can be used as a merge tool for Subversion using this helper: [svn-merge-meld](https://wiki.gnome.org/Apps/Meld?action=AttachFile&do=get&target=svn-merge-meld)
 * Compare selected files by Meld from within a folder: https://code.launchpad.net/~ng-hong-quan/+junk/nautilus-meld


## Getting in Touch

 * [GNOME Discourse](https://discourse.gnome.org/tag/meld)
 * [Report an issue](https://gitlab.gnome.org/GNOME/meld/-/issues/new)


## Development Resources

 * [Issue list](https://gitlab.gnome.org/GNOME/meld/issues)
 * [Git repository browser](https://gitlab.gnome.org/GNOME/meld/tree/master)
 * [Building binaries](Building binaries)
