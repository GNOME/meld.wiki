## Tips and tricks

- Because the standard `Meld.exe` executable doesn't open a terminal, on Windows there's no way to get terminal output from e.g., `--help`. The workaround for this is to use the `MeldConsole.exe` executable, which is also included in the Windows installer. See [this bug](https://gitlab.gnome.org/GNOME/meld/-/issues/91#note_817394) for details.

