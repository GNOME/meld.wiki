This project does not currently support a macOS build,
so we do not supply a pre-compiled binary.

## Execute Meld without compiling

However, as is clear on our project's README,
you may run the project directly without building it first.

These macOS command line instructions assume that you are using [Homebrew](https://brew.sh/) as your package manager.

```zsh
## Cleanup

# first uninstall any older packaged version
brew uninstall meld

## Prerequisites

# now install GTK+ v3 and any dependencies for meld
brew install gtk+3
# GTK+3 will automatically include dependencies like:
# dbus, libxfixes, libxi, libxtst, at-spi2-core, gdk-pixbuf, gsettings-desktop-schemas, hicolor-icon-theme, libepoxy, fribidi, graphite2, icu4c, harfbuzz and pango
brew install gtksourceview4 pygobject3 cairo pkg-config

## Load the source

git clone https://gitlab.gnome.org/GNOME/meld.git

## Runtime execution

cd meld
path=("$(brew --prefix)/bin" $path)
export PATH

bin/meld
```

Note that the options above have been validated on macOS Sonoma 14.3. The runtime execution was tested in a clean environment by beginning with ` env -i $SHELL -f `  then substituting ` path=('/opt/homebrew/bin' $path) `. That substitute path example assumes your processor architecture is Apple Silicon, but you can check it by running ` brew --prefix `.
